(function () {

const ws = new WebSocket("ws://" + window.location.hostname + ":" + (window.location.port || 80) +
"/");
window.addEventListener('load', listAll); //change from init function to this.

function askServer(e){
console.log(e.target.innerHTML);
ws.send(e.target.innerHTML);
}
	
window.filterbtn.addEventListener('click', filterCert);
window.addCertbtn.addEventListener('click', addCert);
window.listAllbtn.addEventListener('click', listAll);
window.updateCerbtn.addEventListener('click', updateCert);
window.deletecertbtn.addEventListener('click', delCert);

function whatServerGotTosay() {
	console.log("sending");
	ws.send("Hello");
	}
	
	function receivedMessageFromServer(myMsg) {
	console.log("receving");
	console.log(myMsg.data);
	// count.innerHTML = myMsg.data;
	}
	
	function init() {
	ws.addEventListener("message", receivedMessageFromServer );
	whatServerGotTosay();
	}
	
	async function listAll() { //loadCar()
	try {
	window.CertTable.innerHTML =('loading');
	let url = '/api/member/certlist'; // let url = '/cars';
	const response = await fetch(url);
	if (!response.ok) throw response;
	putCertsInTable(await response.json());//putCarsInTable(await response.json());
	} catch (e) {
	console.error('error getting pictures', e);
	window.CertTable.innerHTML = "error loading ..";
	
	}
	}
	async function filterCert() {
		try {
		window.CertTable.innerHTML =('loading');
		let url = '/api/member/findcert/';
		url += '?m_cert_by=' + window.m_cert_by.value+"";
		url += '&?m_cert_title=' + window.m_cert_title.value+"";
		url += '&?m_expiry_date=' + window.m_expiry_date.value+"";
		console.log(url);
		const response = await fetch(url);
		if (!response.ok) throw response;
		putCertsInTable(await response.json());
		} catch (e) {
		console.error('error getting pictures', e);
		window.CertTable.innerHTML = "error loadding ..";
		}
		}

async function addCert() {
	try {
	window.CertTable.innerHTML =('loading');
	let url = '/api/member/certadd';
	url += '?m_cert_by=' + window.m_cert_by.value+"";
	url += '&m_cert_title=' + window.m_cert_title.value+"";
	url += '&m_expiry_date=' + window.m_expiry_date.value+"";
	url += '&m_upload_cert=' + window.m_upload_cert.json+"";
	const response = await fetch(url);
	
	if (!response.ok) throw response;
	await listAll();
	} catch (e) {
	console.error('error getting pictures', e);
	window.CertTable.innerHTML = "error loadding ..";
	}
}

async function updateCert() {
	try {
		window.CertTable.innerHTML =('updating');
		let url = '/api/member/updatecert';
		url += '?m_cert_by=' + window.m_cert_by.value+"";
		url += '&m_cert_title=' + window.m_cert_title.value+"";
		url += '&m_expiry_date=' + window.m_expiry_date.value+"";
		console.log(url);
		const response = await fetch(url);
		if (!response.ok) throw response;
		await listAll(); 
		} catch (e) {
		console.error('error getting pictures', e);
		window.CertTable.innerHTML = "error loading";
		}
	}

	async function delCert() {
		try {
			window.CertTable.innerHTML =('updating');
			let url = '/api/member/delcert';
			url += '?m_cert_by=' + window.m_cert_by.value+"";
			url += '&m_cert_title=' + window.m_cert_title.value+"";
			url += '&m_expiry_date=' + window.m_expiry_date.value+"";
			const response = await fetch(url);
			
			if (!response.ok) throw response;
			await listAll(); 
			} catch (e) {
			console.error('error getting pictures', e);
			window.CertTable.innerHTML = "error loadding ..";
			}
		}
		

function putCertsInTable(MCerts) {
	console.log(MCerts);
	let AllRow = "";
	MCerts.forEach((certva) => {
	let newRow ="<tr>" +
	"<td>" + certva.m_cert_by + "</td>" +
	"<td>" + certva.m_cert_title + "</td>" +
	"<td>" + certva.m_expiry_date + "</td>" +
	"<td>" + certva.m_upload_cert + "</td>" +
	"</tr>";
	AllRow += newRow;
	});
	window.CertTable.innerHTML= "<Table border=\"2\">" + AllRow + "</Table>";
	return true;
	}
	}
	());

// function to do the ajax communication with the api created by Vlad
function uploadImage() {
	var formData = new FormData();
	formData.append("profilepic", document.getElementById('profilePic').files[0]);
	fetch('http://localhost:8888/api/member/uploadprofilepic', {
		method: "POST",
		body: formData
	})
	.then(response => response.json())
	.then(response => {
		document.getElementById('image').innerHTML = "";
		var imgEle = document.createElement('img');
		imgEle.src = '../uploads/' + response.fileName;
		document.getElementById('image').appendChild(imgEle);
	});
}