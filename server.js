// configuration
const port = 8888;
const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const ip = require("ip");
const mysql = require('mysql2/');
const multer = require("multer");
const router = express.Router();
const bodyParser = require("body-parser");

var path = require('path');
var session = require('express-session');
var crypto = require('crypto');

var message = "";

// create an express application
// http://expressjs.com/en/api.html
const app = express();
// create an http server that uses express for handling requests
// https://nodejs.org/api/http.html
const server = http.createServer(app);

// Create a WebSocket Server and connect it to the http
// https://github.com/websockets/ws/blob/master/doc/ws.md
const wss = new WebSocket.Server({ server: server });

// Need to set configuration to use globally in this file. created by vlad
var mysqlconfig = {
  host: 'localhost',
  user: 'root',
  password: "",
  database: 'certva'
};
// Create DB Connection with MySql created by vlad
var connection = mysql.createConnection(mysqlconfig);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// serve the client using
// express's default static middleware
app.use(express.static(path.join(__dirname, '/webpages')));

// Session Middleware
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));


// multer is a package that handles file uploads nicely
const uploader = multer({
  dest: './uploads',
  limits: { // for security
    fields: 10,
    fileSize: 1024 * 1024 * 20,
    files: 1,
  },
});

let storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, cb) {
    var fileExt = file.originalname.split('.').reverse()[0];
    cb(null, file.fieldname + '-' + Date.now() + '.' + fileExt);
  }
});

let upload = multer({ storage: storage });

// Start the server
server.listen(port, () => {
  console.log('Server started:', `http://${ip.address()}:${port}`)
});

// Socket Connection
wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    if (message == "Odd")
      ws.send(buildTable(1))
    else if (message == "Even")
      ws.send(buildTable(2))
    else
      ws.send("<tr></tr>");
  });
  console.log("sent back");
});

function buildTable(startno) {
  let retv = "";
  for (let i = startno; i < 31; i += 2) {
    retv += "<tr><td>" + i + "</td></tr>";
  }
  return "<table>" + retv + "</table>";
}

app.get('/', function (request, response) {
  response.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/home', function (request, response) {
  if (request.session.loggedin) {
    response.send('Welcome back, ' + request.session.username + '!');
  } else {
    response.send('Please login to view this page!');
  }
  response.end();
});

/***************************** START API ROUTER *************************************/
/**
 * @url /api/member/certlist
 *
 * @returns JSON
 */
app.get('/api/member/certlist', listAll);
async function listAll(req, res) {
  const query = 'SELECT * FROM member_cert WHERE member_id=' + req.query.member_id;
  connection.query(query, function (err, rows, fields) {
    if (!err) {
      res.json({
        success: true,
        certlist: rows
      });
    } else {
      res.json({
        success: false
      });
    }
  });
};

//create new member to the db
app.post('/api/member/create', function (request, response) {
  var name = request.body.name;
  var niric = request.body.niric;
  var username = request.body.username;
  var password = request.body.password;
  var hashPassword = md5(username + password);
  if ( name && niric && username && password ) {
    connection.query("Insert into member (username, password, name, NRIC) values ('"+ username +"', '"+ hashPassword +"', '"+ name +"', '" + niric + "')", function (err, result) {
      if (err) {
        response.redirect('/register.html');
      }
      if (result) {
        response.redirect('/index.html');
      }
    }
    );
  } else {
    response.send('Please enter Username and Password!');
    response.end();
  }
});

//login api
app.post('/api/login', function (request, response) {
  var username = request.body.username;
  var password = request.body.password;
  var hashPassword = md5(username + password);
  if (username && password) {
    connection.query('SELECT * FROM member WHERE username = ? AND password = ?', [username, hashPassword], function (error, results, fields) {
      if (results.length > 0) {
        request.session.loggedin = true;
        request.session.username = username;
        request.session.member_id = results.insertId;
        response.status(200).send({username: username, msg: true});
      } else {
        console.log("Wrong Username and password");
        response.status(400).send({msg: false});
      }
      response.end();
    });
  } else {
    response.send('Please enter Username and Password!');
    response.end();
  }
});

/**
 *  @url api/memeber/findcert
 *  @description To Find the Certification
 */
app.get('/api/member/findcert', msearchContacts);
async function msearchContacts(req, res) {
  const query = "SELECT * FROM member_cert WHERE m_cert_by LIKE '%" + req.query.m_cert_by + "%' OR m_cert_title LIKE '%"+ req.query.m_cert_title +"%' OR m_expiry_date LIKE '%"+ req.query.m_expiry_date +"%'";
  connection.query(query, function (err, rows, fields) {
    if (!err) {
      res.json({
        success: true,
        certifications: rows
      })
    } else {
      res.json({
        success: false
      });
    }
  });
}

/**
 *  @url api/memeber/certadd
 *  @description To Add the Certification
 */
app.post('/api/member/certadd', addCert);
async function addCert(req, res) {

  var newCert = {};
  newCert.m_cert_by = req.body.m_cert_by;
  newCert.m_cert_title = req.body.m_cert_title;
  newCert.m_expiry_date = req.body.m_expiry_date;
  newCert.m_upload_cert = req.body.m_upload_cert;
  newCert.member_id = req.body.m_member_id;

  const query = connection.format('INSERT INTO member_cert SET ? ;', newCert);

  connection.query(query, function (err, rows, fields) {
    if (!err) {
      res.json({
        success: true,
        member_cert_id: rows.insertId
      });
    } else {
      res.json({ success: false });
    }
  });
}

//NOT WORKING
app.get('/api/member/updatecert', updateCert)
async function updateCert(req, res) {
  const sql = await mysql.createConnection(mysqlconfig);
  console.log(req).query;
  console.log(req.query.m_cert_title);
  const query = 'UPDATE member_cert SET m_expiry_date = ' + req.query.m_cert_title +
    " WHERE m_cert_title =" + req.query.m_cert_title;
  console.log(query);
  await sql.query(query, async function (err, rows, fields) {
    if (!err) {
      res.send("Updated");
    }
    else {
      res.send('Error!');
    }
  });
}

/**
 *  This Code does not work.
 */
// app.get('/api/member/certdelete', delCert);
// async function delCert(req, res) {
//   const sql = await mysql.createConnection(mysqlconfig);
//   console.log(req);
//   var newCert = {};
//   newCert.m_cert_by = req.query.m_cert_by;
//   newCert.m_cert_title = req.query.m_cert_title;
//   newCert.m_expiry_date = req.query.m_expiry_date;
//   const query = 'Delete from member_cert where m_cert_by ="' + req.query.m_cert_by + '"';
//   console.log(query);
//   await sql.query(query, async function (err, rows, fields) {
//     if (!err) {
//       res.send("Deleted");
//     }
//     else {
//       res.send('Error!');
//     }
//   });
// }

/**
 * @url /api/member/certdelete
 * @description delete certification
 */
app.delete('/api/member/certdelete', certdelete);
async function certdelete(req, res) {
  const query  = 'DELETE from member_cert where id="' + req.body.m_cert_id + '"';
  connection.query(query, function (err, rows, fields) {
    if (!err) {
      res.json({
        success: true
      });
    } else {
      res.json({
        success: false
      });
    }
  });
}

// Upload Certification API created by vlad
app.post('/api/ca/uploadcertprof', upload.single('certification'), function (req, res) {
  if (!req.file) {
    console.log("No file received");
    message = "Error! in image upload."
    res.status(400).sendFile(path.join(__dirname + '/ca_mgmt.html'));
  } else {
    res.status(200).json({msg: message, fileName: req.file.filename});
  }
});


//api to upload the image , created by Vlad
app.post('/api/member/uploadprofilepic', upload.single('profilepic'), function (req, res) {
  if (!req.file) {
    console.log("No file received");
    message = "Error! in image upload."
    res.status(400).sendFile(path.join(__dirname + '/create_member.html'));
  } else {
    // Sql query was not exact. Compare what you have done.
    var sql = "INSERT INTO member (`profile_photo`) VALUES ('" + req.file.filename + "')";

    connection.query(sql, function (err, result) {
      if (err) {
        console.log("Error:" + err);
        return false;
      }
      message = "Successfully! uploaded";
      res.json({ msg: message, fileName: req.file.filename });
    });
  }
});

app.get('/cacert', load1Cert);
async function load1Cert(req, res) {
  var pathModule = "/texttools.js";
  var pathLoadMe = "/certlist.txt";
  var tools = require(__dirname + pathModule);
  var arr = (tools.fileToArray(__dirname + pathLoadMe));
  var retv = [];
  arr.forEach((line) => {
    var partOf = line.split(",");
    var thisCert = {};
    thisCert.name = partOf[0];
    thisCert.age = partOf[1];
    retv.push(thisCert);
  });
  res.send(retv);
}

app.get('/api/ca/certlist', calistAll); //app.get('/cars', listAll);
async function calistAll(req, res) { //async function listAllCars(req, res) {
  connection.query('SELECT * from cert_auth', function (err, rows, fields) {
    if (!err) {
      res.send(rows);
    }
    else {
      res.send('Error!');
    }
  });
};

app.get('/api/ca/findcert', searchContacts); //app.get('/carsFind', searchContacts);
async function searchContacts(req, res) {
  const sql = await mysql.createConnection(mysqlconfig);
  console.log(req);
  const query = sql.format(
    `SELECT cert_code,cert_title FROM cert_auth
WHERE cert_code LIKE ? OR cert_title LIKE ?
ORDER BY cert_code`, [req.query.cert_code, req.query.cert_title]);
  console.log(query);
  await sql.query(query, function (err, rows, fields) {
    if (!err) {
      res.send(rows);
    }
    else {
      res.send('Error!');
    }
  });
}

app.get('/api/ca/certadd', caaddCert); //app.get('/New', insertNewCar);
async function caaddCert(req, res) { //async function insertNewCar(req, res) {
  const sql = await mysql.createConnection(mysqlconfig);
  console.log(req);
  var canewCert = {}; //var newCar = {};
  canewCert.cert_code = req.query.cert_code; //newCar.reg = req.query.reg;
  canewCert.cert_title = req.query.cert_title;
  const query = sql.format('INSERT INTO cert_auth SET ? ;', canewCert);
  console.log(query);
  await sql.query(query, async function (err, rows, fields) {
    if (!err) {
      res.send("Inserted");
    }
    else {
      res.send('Error!');
    }
  });
}

//NOT WORKING
app.get('/api/ca/updatecert', caupdateCert)
async function caupdateCert(req, res) {
  const sql = await mysql.createConnection(mysqlconfig);
  console.log(req).query;
  console.log(req.query.cert_code);
  const query = 'UPDATE cert_auth SET cert_title = ' + req.query.cert_title +
    " WHERE cert_code =" + req.query.m_cert_code;
  console.log(query);
  await sql.query(query, async function (err, rows, fields) {
    if (!err) {
      res.send("Updated");
    }
    else {
      res.send('Error!');
    }
  });
}

app.get('/api/ca/delcert', cadeleteCert);
async function cadeleteCert(req, res) {
  const sql = await mysql.createConnection(mysqlconfig);
  console.log(req);
  var canewCert = {};
  canewCert.m_cert_by = req.query.m_cert_by;
  canewCert.m_cert_title = req.query.m_cert_title;
  const query = 'Delete from cert_auth where cert_code ="' + req.query.cert_code + '"';
  console.log(query);
  await sql.query(query, async function (err, rows, fields) {
    if (!err) {
      res.send("Deleted");
    }
    else {
      res.send('Error!');
    }
  });
}
/**************************** END API ROUTER **********************************/

/**************************** UTILITY FUNCTIONs *******************************/
//make hash password
function md5(string) {
  return crypto.createHash('md5').update(string).digest('hex');
}